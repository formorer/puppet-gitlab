class gitlab::redis inherits gitlab {
  if $gitlab::redis_manage {

    ensure_packages ( "redis-server", { ensure => 'installed' })

    service { 'redis-server':
      ensure  => 'running',
      enable  => true,
      require => Package['redis-server'],
    }

    file { "/etc/redis/redis.conf":
      owner => redis,
      mode => "640",
      group => redis,
      source => "puppet:///modules/gitlab/redis.conf",
      notify  => Service['redis-server'],
      require => Package['redis-server'],
    }

    file { "/var/run/redis":
      ensure => "directory",
      owner => redis,
      group => redis,
      mode => "750",
      notify  => Service['redis-server'],
    }
  }

}
