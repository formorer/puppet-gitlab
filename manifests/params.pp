class gitlab::params {
  $package_manage = true
  $user_manage = false
  $user = "git"
  $group = "git"
  $home = "/srv/git"
  $yarnversion = "v0.28.4"
  $yarnchecksum = "057ef781107bb5d3e7a2a655d75054fbeb265a249a905375bc25bec10d42b31"
  $redis_manage = true
  $source_manage = false
  $source_version = "9-5-stable"
}
