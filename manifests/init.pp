# Class: gitlab
# ===========================
#
# Full description of class gitlab here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'gitlab':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2017 Your name here, unless otherwise noted.
#
class gitlab (
  $package_manage = $gitlab::params::package_manage,
  $user_manage = $gitlab::params::user_manage,
  $source_manage = $gitlab::params::source_manage,
  $source_version = $gitlab::params::source_version,
  $user = $gitlab::params::user,
  $group = $gitlab::params::group,
  $home = $gitlab::params::home,
  $yarnversion = $gitlab::params::yarnversion,
  $yarnchecksum = $gitlab::param::yarnchecksum,
  $redis_manage = $gitlab::params::redis_manage
) inherits gitlab::params {

  # anchor things in correct order
  anchor { 'gitlab::begin': } ->
  class { '::gitlab::redis': } ->
  class { '::gitlab::user': } ->
  class { '::gitlab::install': } ->
  anchor { 'gitlab::end': }

}
