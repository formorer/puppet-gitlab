class gitlab::user inherits gitlab {

  if $gitlab::user_manage {
    group { $gitlab::group:
      ensure => 'present',
    }

    user { $gitlab::user:
      ensure => 'present',
      gid    => $gitlab::group,
      home   => $gitlab::home,
      shell  => "/bin/bash",
      groups => ["redis"],
    }
  }

  file { $gitlab::home:
    owner  => $gitlab::user,
    group  => $gitlab::group,
    mode   => '0755',
    ensure => 'directory',
  }
}
